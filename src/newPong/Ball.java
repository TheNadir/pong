package newPong;


import java.awt.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Ball extends Rectangle {
    
    //Global Variables
    int xSpeed = 1;
    int ySpeed = 1;
    
    
    public Ball(int x, int y, int width, int height, Color color) {
        super(x, y, width, height, color);
    }
    
    public Ball(int x, int y, int width, int height) {
        super(x, y, width, height);
    }
    
    public void update() {
        move(xSpeed, ySpeed);
        
        if (getX() < 0) {                   //hit left wall - Player 1 wins
            xSpeed = 1;
        }
        if (getX() + getWidth() > 590) {    //hit right wall - Player 2 wins
            xSpeed = -1;
        }
        
        if (getY() < 0) {
            ySpeed = 1;
        }
        if (getY() + getHeight() > 370) {
            ySpeed = -1;
        }
        
    }
    
    public void move(int x, int y) {
        setX(getX() + x);
        setY(getY() + y);
    }
    
    /**
     * invert the speed when ball hits player
     */
    public void invertXSpeed() {
        xSpeed *= -1;
    }
    
    /**
     * invert the speed when ball hits player
     */
    public void invertYSpeed() {
        ySpeed *= -1;
    }
    
    /**
     * invert both speeds when ball hits player
     */
    public void invertSpeed() {
        xSpeed *= -1;
        //ySpeed *= -1;
    }
    
    public int winPoint() {
        if (getX() < 0) {                   //hit left wall - Player 1 win
            return 1;
        }
        if (getX() + getWidth() > 590) {     //hit right wall - Player 2 win
            return 2;
        }
        
        return 0;           //noone gets a point
    }
}
