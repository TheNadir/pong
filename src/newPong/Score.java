/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newPong;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Score extends Element {
    
    //Global Variables
    private int score = 0;
    
    public Score(int x, int y, int width, int height, Color color) {
        super(x, y, width, height, color);
    }
    
    public Score(int x, int y, int width, int height) {
        super(x, y, width, height);
    }
    
    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        Font font = new Font("Serif", Font.PLAIN, 40);
        g.setFont(font);
        g.drawString(String.valueOf(score), getX(), getY());
    }
    
    
    //Getters and Setters
    public int getScore() {
        return score;
    }
    public void setScore(int newScore) {
        score = newScore;
    }
    public void itterateScore() {
        score ++;
    }
}
