package newPong;


import java.awt.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Player extends Rectangle {
    
    //Global Variables
    private final int moveLength = 2;
    
    
    public Player(int x, int y, int width, int height, Color color) {
        super(x, y, width, height, color);
    }
    
    public Player(int x, int y, int width, int height) {
        super(x, y, width, height);
    }
    
    public void move(boolean up) {
        if (up) {
            if (getY() >= 0) {                  //don't let Player go above window
                setY(getY() - moveLength);
            }
        }
        else {
            if (getY() + getHeight() + getHeight() <= 400) {  //don't let Play go below window
                setY(getY() + moveLength);
            }
        }
    }
}
