package newPong;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class PongFrame extends JPanel implements Runnable {
    
    //Global Variables
    private final static int MOVING_UP = 1;
    private final static int MOVING_NULL = 0;
    private final static int MOVING_DOWN = -1;
    
    private static boolean startGame = false;
    private Ball ball;
    private static Player player1;
    private static Player player2;
    private static int player1moving = MOVING_NULL;
    private static int player2moving = MOVING_NULL;
    private static Collection <Element> elements = new ArrayList<>();
    private Score[] scores = new Score[2];
    private static Text startText;
    private Element background;
    private static final double ORIGINAL_FPS = 200 / 36;
    private static double FPS = 200 / 36;
    private final static int WIDTH = 600;
    private final static int HEIGHT = 400;
    private final int PLAYER_WIDTH = 15;
    private final int PLAYER_HEIGHT = 60;
    
    
    public static void main(String[] args) {
        JFrame parentWindow = new JFrame("Pong");
        
        parentWindow.getContentPane().add(new PongFrame());
        parentWindow.addKeyListener(new keyListener());
        
        parentWindow.setSize(WIDTH, HEIGHT);
        parentWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        parentWindow.setVisible(true);
    }
    
    public PongFrame() {
        init();
        
        new Thread(this).start();
    }
    
    public void init() {
        int playerStartY = (HEIGHT / 2) - (PLAYER_HEIGHT / 2);
        scores[0] = new Score(WIDTH/2+80, 50, 100, 100, Color.WHITE);
        scores[1] = new Score(WIDTH/2-100, 50, 100, 100, Color.WHITE);
        startText = new Text(120, 100, 0, 0, Color.WHITE, "Press SPACE to start.");
        
        elements.add(background = new Rectangle(0, 0, WIDTH, HEIGHT, Color.BLACK));
        elements.add(ball = new Ball(WIDTH/2, HEIGHT/2, 20, 20));
        elements.add(player1 = new Player(WIDTH - 50, playerStartY, PLAYER_WIDTH, PLAYER_HEIGHT));
        elements.add(player2 = new Player(15, playerStartY, PLAYER_WIDTH, PLAYER_HEIGHT));
        elements.add(scores[0]);
        elements.add(scores[1]);
        elements.add(startText);
    }
    
    public void run() {
        //Remember the current time
        long tm = System.currentTimeMillis();
        
        while (true){
            update();
            repaint();
            
            if (ball.isColliding(player1)) {
                ball.invertSpeed();
                System.out.println("Player 1");
                update();
                FPS -= 0.1;                         //make ball go faster
            }
            if (ball.isColliding(player2)) {
                ball.invertSpeed();
                System.out.println("Player 2");
                update();
                FPS -= 0.1;                         //make ball go faster
            }
            
            if (ball.winPoint() != 0) {
                scores[ball.winPoint()-1].itterateScore();
                ball.setX(WIDTH/2);
                ball.setY(HEIGHT/2);
                FPS = ORIGINAL_FPS;             //reset the ball speed
            }
            
            
            //Player movements
            if (player1moving == MOVING_UP) {
                player1.move(true);
            }
            else if (player1moving == MOVING_DOWN) {
                player1.move(false);
            }
            
            if (player2moving == MOVING_UP) {
                player2.move(true);
            }
            else if (player2moving == MOVING_DOWN) {
                player2.move(false);
            }
            
            
            
            try {
                tm += FPS;
                Thread.sleep(Math.max(0, tm - System.currentTimeMillis()));
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
    }
    
    public void update() {
        if (startGame) {
            ball.update();
        }
    }
    
    public void paint(Graphics g) {
        Iterator iterator = elements.iterator();
        
        while (iterator.hasNext()) {
            ((Element) iterator.next()).draw(g);
        }
    }
    
    
    
    //Key Events
    public static class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_O) {
                player1moving = MOVING_UP;
            }
            else if (ke.getKeyCode() == KeyEvent.VK_L) {
                player1moving = MOVING_DOWN;
            }
            
            if (ke.getKeyCode() == KeyEvent.VK_W) {
                player2moving = MOVING_UP;
            }
            else if (ke.getKeyCode() == KeyEvent.VK_S) {
                player2moving = MOVING_DOWN;
            }
            
            if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
                startGame = true;
                startText.setText("");
            }
            
            //Exit on "esc"
            if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
                System.exit(0);
            }
        }
        
        @Override
        public void keyReleased(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_O || ke.getKeyCode() == KeyEvent.VK_L) {
                player1moving = MOVING_NULL;
            }
            
            if (ke.getKeyCode() == KeyEvent.VK_W || ke.getKeyCode() == KeyEvent.VK_S) {
                player2moving = MOVING_NULL;
            }
        }
    }
}
