/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newPong;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 *
 * @author Nikhil Sinha sinhanr@mail.uc.edu
 */
public class Text extends Element {
    
    //Global Variables
    private String text = "DEFAULT TEXT";
    
    public Text(int x, int y, int width, int height, Color color, String newText) {
        super(x, y, width, height, color);
        text = newText;
    }
    
    public Text(int x, int y, int width, int height, String newText) {
        super(x, y, width, height);
        text = newText;
    }
    
    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        Font font = new Font("Serif", Font.PLAIN, 40);
        g.setFont(font);
        g.drawString(String.valueOf(text), getX(), getY());
    }
    
    
    //Getters and Setters
    public String getText() {
        return text;
    }
    public void setText(String newText) {
        text = newText;
    }
}
